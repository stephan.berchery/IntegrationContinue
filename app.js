var express = require('express');
var app = express();
var hostname = process.env.API_HOST | 'localhost';
var port = process.env.API_PORT || 10210;

app.get('/substraction/sub', function(req, res){

    var op1 = req.query.operand1 || 0;
    var op2 = req.query.operand2 || 0;
    
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.send(""  + (Number(op1) - Number(op2)));
});

var server = app.listen(port, function() {
    console.log('Listening at http://localhost:' + port);
});

