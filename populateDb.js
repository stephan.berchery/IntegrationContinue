'use strict';
var Course        = require('./app/models/course');
var Training        = require('./app/models/training');
var User        = require('./app/models/user');
var Video        = require('./app/models/video');
/**
 * Check if Collection are empty and populate them if yes
 * @param db
 */
function createCourses(db) {
    Course.find(function (err,course) {
        if(course.length == 0){
            console.log("DB collection Courses empty");
            var course = new Course({"_id":"58f922b5392eb520dce131f8","title":"ANGL801","details":"Anglais","videos":["5904fdcf3b8c5b192aee13b4"]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce131fc","title":"INFO806","details":"Application mobiles","videos":["5904fdb23b8c5b192aee13b3", "5904fcff3b8c5b192aee13b2"]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce131fa","title":"INFO805","details":"Infographie","videos":["5904fce43b8c5b192aee13b1", "59033b847f3c927a27188fe9"]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce131fb","title":"INFO804","details":"Ergonomie","videos":["59034159ebbdf7014112cc2d"]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce131f9","title":"PROJ801","details":"Projet","videos":["59033da12273d27c207a45ef"]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce131fe","title":"ANGL701","details":"Anglais","videos":[]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce13201","title":"INFO706","details":"IDE","videos":[]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce13200","title":"INFO704","details":"Analyse d'algorithmes","videos":[]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce131fd","title":"INFO803","details":"Parallèlisme","videos":["58e9ead759c07c4b0b1bae8d"]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce131ff","title":"INFO705","details":"Architectures logicielles","videos":[]});
            course.save();
            course = new Course({"_id":"58f922b5392eb520dce13202","title":"INFO703","details":"Compilation","videos":[]});
            course.save();
            console.log("DB collection Courses Create");
        }
    })

}

function createVideos(db) {
    Video.find(function (err,video) {
        if(video.length == 0){
            console.log("DB collection Videos empty");
            var video = new Video({"_id": "5904fdcf3b8c5b192aee13b4", "tags": "ergo", "owner": "58e9ead759c07c4b0b1bae87", "created": "2017/04/29 22:55:43", "url": "http://193.48.123.188:8080/api/medias/file-1493499316411.mp4", "comment": "ergo", "title": "ergo", "thumbnail": "http://193.48.123.188:8080/api/medias/file-8.png", "__v": 0, "nbLikes": []});
            video.save();
            video = new Video({"_id": "5904fdb23b8c5b192aee13b3", "tags": "ergo", "owner": "58e9ead759c07c4b0b1bae87", "created": "2017/04/29 22:55:14", "url": "http://193.48.123.188:8080/api/medias/file-1493499235486.mp4", "comment": "ergo", "title": "ergo", "thumbnail": "http://193.48.123.188:8080/api/medias/file-7.png", "__v": 0, "nbLikes": []});
            video.save();
            video = new Video({"_id": "5904fcff3b8c5b192aee13b2", "tags": "ergo", "owner": "58e9ead759c07c4b0b1bae87", "created": "2017/04/29 22:52:15", "url": "http://193.48.123.188:8080/api/medias/file-1493499109311.mp4", "comment": "ergo", "title": "ergo", "thumbnail": "http://193.48.123.188:8080/api/medias/file-6.png", "__v": 0, "nbLikes": []});
            video.save();
            video = new Video( {"_id": "5904fce43b8c5b192aee13b1", "tags": "test", "owner":"58e9ead759c07c4b0b1bae87", "created": "2017/04/29 22:51:48", "url": "http://193.48.123.188:8080/api/medias/file-1493499081383.mp4", "comment": "test", "title": "test", "thumbnail": "http://193.48.123.188:8080/api/medias/file-5.png", "__v": 0, "nbLikes": []});
            video.save();
            video = new Video({"_id": "59034159ebbdf7014112cc2d", "tags": "demo", "owner": "58e9ead759c07c4b0b1bae87", "created": "2017/04/28 15:19:21", "url": "http://193.48.123.188:8080/api/medias/file-1493385531124.mp4", "comment": "demo", "title": "demo", "thumbnail": "http://193.48.123.188:8080/api/medias/file-4.png", "__v": 0, "nbLikes": []});
            video.save();
            video = new Video({"_id": "59033da12273d27c207a45ef", "tags": "ergo", "owner" : "58e9ead759c07c4b0b1bae87", "created": "2017/04/28 15:03:29", "url": "http://193.48.123.188:8080/api/medias/file-1493384579682.mp4", "comment": "test upload", "title": "upload", "thumbnail": "http://193.48.123.188:8080/api/medias/file-2.png", "__v": 0, "nbLikes": []});
            video.save();
            video = new Video({"_id": "59033b847f3c927a27188fe9", "tags": "test", "owner" : "58e9ead759c07c4b0b1bae87", "created": "2017/04/28 14:54:28", "url": "http://193.48.123.188:8080/api/medias/file-1493384067047.mp4", "comment": "test", "title": "test", "thumbnail": "http://193.48.123.188:8080/api/medias/file-1.png", "__v": 0, "nbLikes": []});
            video.save();
            video = new Video({"_id": "58e9ead759c07c4b0b1bae8d", "title": "Privacy issues", "created": "2017/02/26 13:21:41", "owner" : "58e9ead759c07c4b0b1bae87", "comment": "blablablablabla", "url": "http://193.48.123.188:8080/api/medias/file-1493384067047.mp4", "tags": "English", "thumbnail": "http://193.48.123.188:8080/api/medias/file-1.png", "__v": 1, "nbLikes": []});
            video.save();
            console.log("DB collection Videos Create");
        }
    })

}

function createUsers(db) {
    User.find(function (err,user) {
        if(user.length == 0){
            console.log("DB collection Users empty");
            var user = new User({"_id":"58e9ead759c07c4b0b1bae89", "username" : "Marien", "email":"marienleblanc@gmail.com","password":"password","role":"etudiant","created":"2017/02/26 13:21:41","gravatar":"http://www.gravatar.com/avatar/511bc1eed6f69858ccdf535e4532c4be?d=identicon&s=48","training":"58e9ead759c07c4b0b1bae83","favorites":[]});
            user.save();
            user = new User({"_id":"58e9ead759c07c4b0b1bae88","username" : "Luis", "email":"luis@gmail.com","password":"password","role":"etudiant","created":"2017/02/26 13:21:41","gravatar":"http://www.gravatar.com/avatar/511bc1eed6f69858ccdf535e4532c4be?d=identicon&s=48","training":"58e9ead759c07c4b0b1bae83","favorites":[]});
            user.save();
            user = new User({"_id":"58e9ead759c07c4b0b1bae87","username" : "Stéphan", "email":"stephan.berchery@gmail.com","password":"password","role":"etudiant","created":"2017/02/26 13:21:41","gravatar":"http://www.gravatar.com/avatar/511bc1eed6f69858ccdf535e4532c4be?d=identicon&s=48","training":"58e9ead759c07c4b0b1bae83","__v":2,"favorites":["59033b847f3c927a27188fe9"]});
            user.save();
            user = new User({"_id":"58e9ead759c07c4b0b1bae8c","username" : "Prof", "email":"prof@prof.com","password":"password","role":"enseignant","created":"2017/02/26 13:21:41","gravatar":"http://www.gravatar.com/avatar/511bc1eed6f69858ccdf535e4532c4be?d=identicon&s=48","training":"58e9ead759c07c4b0b1bae83","favorites":[]});
            user.save();
            user = new User({"_id":"58e9ead759c07c4b0b1bae8a","username" : "Oury", "email":"oury.diallo@gmail.com","password":"password","role":"etudiant","created":"2017/02/26 13:21:41","gravatar":"http://www.gravatar.com/avatar/511bc1eed6f69858ccdf535e4532c4be?d=identicon&s=48","training":"58e9ead759c07c4b0b1bae83","favorites":[]});
            user.save();
            console.log("DB collection Users Create");
        }
    })

}

function createTrainings(db) {
    Training.find(function (err,training) {
        if(training.length == 0){
            console.log("DB collection Trainings empty");
            training = new Training({"_id":"58e9ead759c07c4b0b1bae84","title":"M2-S9-GEOSPHERES","details":"Master2 Géographie semestre 9","courses":[]});
            training.save();
            training = new Training({"_id":"58e9ead759c07c4b0b1bae82","title":"M1-S7-ISC","details":"Master1 Informatique semestre 7","courses":[]});
            training.save();
            training = new Training({"_id":"58e9ead759c07c4b0b1bae86","title":"L3-S6-MATH","details":"Licence3 Mathématiques semestre 6","courses":[]});
            training.save();
            training = new Training({"_id":"58e9ead759c07c4b0b1bae85","title":"STAPS-S3-L2","details":"Licence2 STAPS semestre 3","courses":[]});
            training.save();
            training = new Training({"_id":"58e9ead759c07c4b0b1bae83","title":"M1-S8-ISC","details":"Master1 Informatique semestre 8","__v":4,"courses":["58f922b5392eb520dce131f8","58f922b5392eb520dce131fc","58f922b5392eb520dce131fa","58f922b5392eb520dce131fb","58f922b5392eb520dce131f9", "58f922b5392eb520dce131fd"]});
            training.save();
            console.log("DB collection Trainings Create");
        }
    })

}

module.exports = {
    createCourses: createCourses,
    createVideos: createVideos,
    createUsers: createUsers,
    createTrainings: createTrainings,
};