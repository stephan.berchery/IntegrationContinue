var express = require('express');
var Course        = require('./../models/course');

var courses_router = express.Router();

var ffmpeg = require('ffmpeg');

// on routes that end in /courses
// ----------------------------------------------------
courses_router.route('/')

// create a course (accessed at POST http://localhost:8080/api/courses)
    .post(function(req, res) {

        var course = new Course();      // create a new instance of the course model
        course.title = req.body.title;
        course.details = req.body.details;
        course.videos.push(req.body.videos);

        // save the course and check for errors
        course.save(function(err) {
            if (err)
                res.send(err);

            res.json(course);
        });

    })

    // get all the courses (accessed at GET http://localhost:8080/api/courses)
    .get(function(req, res) {
        Course.find(function(err, courses) {
            if (err)
                res.send(err);

            res.json(courses);
        });
    });

// on routes that end in /courses/:course_id
// ----------------------------------------------------
courses_router.route('/:course_id')

// get the course with that id (accessed at GET http://localhost:8080/api/courses/:course_id)
    .get(function(req, res) {
        console.log("get course by id")
        Course.findById(req.params.course_id, function(err, course) {
            if (err)
                res.send(err);
            res.json(course);
        })
            .populate("videos");
    })

    // update the course with this id (accessed at PUT http://localhost:8080/api/courses/:course_id)
    .put(function(req, res) {

        // use our course model to find the course we want
        Course.findById(req.params.course_id, function(err, course) {

            if (err)
                res.send(err);

            course.title = req.body.title;
            course.details = req.body.details;
            course.videos.push(req.body.videos);

            // save the course
            course.save(function(err) {
                if (err)
                    res.send(err);

                res.json(course);
            });

        });
    })

    // update the course with this id (accessed at PATCH http://localhost:8080/api/courses/:course_id)
    .patch(function(req, res) {

        // use our course model to find the course we want
        Course.findById(req.params.course_id, function(err, course) {

            for(var key in req.body) {
                if(req.body.hasOwnProperty(key)){
                    course[key] = req.body[key];
                }
            }

            if (err)
                res.send(err);

            // save the course
            course.save(function(err) {
                if (err)
                    res.send(err);

                res.json(course);
            });

        });
    })

    // delete the course with this id (accessed at DELETE http://localhost:8080/api/courses/:course_id)
    .delete(function(req, res) {
        Course.remove({
            _id: req.params.course_id
        }, function(err) {
            if (err)
                res.send(err);

            res.json(req.params.course_id);
        });
    });
// on routes that end in /courses/:course_id/addvideo
// ----------------------------------------------------
courses_router.route('/:course_id/addvideo')

// post the course with that id to add or delete a favorite(accessed at GET http://localhost:8080/api/courses/:course_id/addvideo) Send jsonObject : {"videos" : "video_id"}
    .post(function(req, res) {
        Course.findById(req.params.course_id, function(err, course) {

            if (err)
                res.send(err);

            var index = course.videos.indexOf(req.body.videos);
            if(index == -1){
                course.videos.push(req.body.videos);
            }else{
                course.videos.remove(course.videos[index]);
            }

            // save the course and check for errors
            course.save(function(err) {
                if (err)
                    res.send(err);
                res.json(course);
            });
        });
    })
module.exports = courses_router;