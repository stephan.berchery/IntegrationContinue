var express = require('express');
var multer = require('multer');
const path = require('path');
const fs = require('fs');
const ytdl = require('ytdl-core');
var ffmpeg = require('ffmpeg');
var thumbler = require('video-thumb');

var medias_router = express.Router();

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './medias/');
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
    }
});

var upload = multer({ //multer settings
    storage: storage
}).single('file');

// on routes that end in /medias
// ----------------------------------------------------
medias_router.route('/')

// create a media (accessed at POST http://localhost:8080/api/medias)
    .post(function(req, res) {
        upload(req,res,function(err){
            console.log(req.file);
            if(err){
                res.json({error_code:1,err_desc:err});
                return;
            }
            var thumbname = 'thumb-' + Date.now() + '.png';
            thumbler.extract(path.resolve(__dirname, '../../medias', req.file.filename), path.resolve(__dirname, '../../medias', thumbname), '00:00:01', '200x125', function(){
                console.log('snapshot saved');
                console.log(path.resolve(__dirname, '../../medias', thumbname))
            });
            res.json({error_code:0,err_desc:null, filename:req.file.filename, thumbnail:thumbname});
        });

    });

// on routes that end in /medias/url
// ----------------------------------------------------
medias_router.route('/url')

// create a media (accessed at POST http://localhost:8080/api/medias/url)
    .post(function(req, res) {
        console.log(req.body.url);
        var filename = 'video-' + Date.now() + '.mp4';
        var output = path.resolve(__dirname, '../../medias', filename);
        var thumbname = 'thumb-' + Date.now() + '.png';

        var video = ytdl(req.body.url);
        video.pipe(fs.createWriteStream(output));

        video.on('end', function() {
            thumbler.extract(output, path.resolve(__dirname, '../../medias', thumbname), '00:00:20', '200x125', function(){
                console.log('snapshot saved');
                console.log(path.resolve(__dirname, '../../medias', thumbname))
            });
        });

        res.json({filename:filename, thumbnail:thumbname});
    });

// on routes that end in /medias/:media_id
// ----------------------------------------------------
medias_router.route('/:media_id')
// get the media with that id (accessed at GET http://localhost:8080/api/medias/:media_id)
    .get(function(req, res) {
        var options = {root: './medias/'};
        res.sendFile(req.params.media_id, options)
    });

module.exports = medias_router;