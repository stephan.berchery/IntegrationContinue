var express = require('express');
var Video        = require('./../models/video');
var dateFormat = require('dateformat');

var videos_router = express.Router();

// on routes that end in /videos
// ----------------------------------------------------
videos_router.route('/')

// create a video (accessed at POST http://localhost:8080/api/videos)
    .post(function(req, res) {

        var video = new Video();      // create a new instance of the video model
        video.title = req.body.title;
        video.comment = req.body.comment;
        video.url = req.body.url;
        video.thumbnail = req.body.thumbnail;
        video.created = dateFormat(new Date(), "yyyy/mm/dd HH:MM:ss");
        //video.owner = req.body.owner._id;
        video.owner = req.body.owner;
        video.tags = req.body.tags;
        video.nbLikes = [];

        // save the video and check for errors
        video.save(function(err) {
            if (err)
                res.send(err);

            res.json(video);
        });

    })

    // get all the videos (accessed at GET http://localhost:8080/api/videos)
    .get(function(req, res) {
        Video.find(function(err, videos) {
            if (err)
                res.send(err);

            res.json(videos);
        }).sort({created: 'desc'})
            .populate("owner");
    });

// on routes that end in /videos/:video_id
// ----------------------------------------------------
videos_router.route('/:video_id')

// get the video with that id (accessed at GET http://localhost:8080/api/videos/:video_id)
    .get(function(req, res) {
        Video.findById(req.params.video_id, function(err, video) {
            if (err)
                res.send(err);
            res.json(video);
        })
            .populate("owner");
    })

    // update the video with this id (accessed at PUT http://localhost:8080/api/videos/:video_id)
    .put(function(req, res) {

        // use our video model to find the video we want
        Video.findById(req.params.video_id, function(err, video) {

            if (err)
                res.send(err);

            video.title = req.body.title;
            video.comment = req.body.comment;
            video.url = req.body.url;
            video.thumbnail = req.body.thumbnail;
            video.created = req.body.created;
            video.owner = req.body.owner._id;
            video.tags = req.body.tags;
            video.nbLikes = req.body.nbLikes;

            // save the video
            video.save(function(err) {
                if (err)
                    res.send(err);

                res.json(video);
            });

        });
    })

    // update the video with this id (accessed at PATCH http://localhost:8080/api/videos/:video_id)
    .patch(function(req, res) {

        // use our video model to find the video we want
        Video.findById(req.params.video_id, function(err, video) {

            for(var key in req.body) {
                if(req.body.hasOwnProperty(key)){
                    video[key] = req.body[key];
                }
            }

            if (err)
                res.send(err);

            // save the video
            video.save(function(err) {
                if (err)
                    res.send(err);

                res.json(video);
            });

        });
    })

    // delete the video with this id (accessed at DELETE http://localhost:8080/api/videos/:video_id)
    .delete(function(req, res) {
        Video.remove({
            _id: req.params.video_id
        }, function(err) {
            if (err)
                res.send(err);

            res.json(req.params.video_id);
        });
    });

// on routes that end in /videos/owner/user_id
// ----------------------------------------------------
videos_router.route('/owner/:user_id')

// get the video with owner user_id(accessed at GET http://localhost:8080/api/videos/owner/:user_id)
    .get(function(req, res) {
        Video.find({ owner : req.params.user_id }, function (err, video) {
            if (err)
                res.send(err);
            res.json(video);
        });
    });

module.exports = videos_router;