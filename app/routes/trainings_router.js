var express = require('express');
var Training        = require('./../models/training');

var trainings_router = express.Router();

// on routes that end in /trainings
// ----------------------------------------------------
trainings_router.route('/')

// create a training (accessed at POST http://localhost:8080/api/trainings)
    .post(function(req, res) {

        var training = new Training();      // create a new instance of the training model
        training.title = req.body.title;
        training.details = req.body.details;
        training.courses = req.body.courses;

        // save the training and check for errors
        training.save(function(err) {
            if (err)
                res.send(err);

            res.json(training);
        });

    })

    // get all the trainings (accessed at GET http://localhost:8080/api/trainings)
    .get(function(req, res) {
        Training.find(function(err, trainings) {
            if (err)
                res.send(err);

            res.json(trainings);
        });
    });

// on routes that end in /trainings/:training_id
// ----------------------------------------------------
trainings_router.route('/:training_id')

// get the training with that id (accessed at GET http://localhost:8080/api/trainings/:training_id)
    .get(function(req, res) {
        console.log("get training by id")
        Training.findById(req.params.training_id, function(err, training) {
            if (err)
                res.send(err);
            res.json(training);
        })
            .populate("courses");
    })

    // update the training with this id (accessed at PUT http://localhost:8080/api/trainings/:training_id)
    .put(function(req, res) {

        // use our training model to find the training we want
        Training.findById(req.params.training_id, function(err, training) {

            if (err)
                res.send(err);

            training.title = req.body.title;
            training.details = req.body.details;
            training.courses.push(req.body.courses);

            // save the training
            training.save(function(err) {
                if (err)
                    res.send(err);

                res.json(training);
            });

        });
    })

    // update the training with this id (accessed at PATCH http://localhost:8080/api/trainings/:training_id)
    .patch(function(req, res) {

        // use our training model to find the training we want
        Training.findById(req.params.training_id, function(err, training) {

            for(var key in req.body) {
                if(req.body.hasOwnProperty(key)){
                    training[key] = req.body[key];
                }
            }

            if (err)
                res.send(err);

            // save the training
            training.save(function(err) {
                if (err)
                    res.send(err);

                res.json(training);
            });

        });
    })

    // delete the training with this id (accessed at DELETE http://localhost:8080/api/trainings/:training_id)
    .delete(function(req, res) {
        Training.remove({
            _id: req.params.training_id
        }, function(err) {
            if (err)
                res.send(err);

            res.json(req.params.training_id);
        });
    });

module.exports = trainings_router;