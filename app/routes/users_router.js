var express = require('express');
var User        = require('./../models/user');
var dateFormat = require('dateformat');
var md5 = require("md5");

var users_router = express.Router();

// on routes that end in /users
// ----------------------------------------------------
users_router.route('/')

// create a user (accessed at POST http://localhost:8080/api/users)
    .post(function(req, res) {

        var user = new User();      // create a new instance of the User model
        user.password = req.body.password;
        user.email = req.body.email;
        user.username = req.body.username;
        user.role = req.body.role;
        user.created = dateFormat(new Date(), "yyyy/mm/dd HH:MM:ss");
        user.gravatar = "http://www.gravatar.com/avatar/" + md5(user.email) + "?d=identicon&s=48";
        //user.training = req.body.training._id;
        user.training = req.body.training;
        user.favorites = [];

        // save the user and check for errors
        user.save(function(err) {
            if (err)
                res.send(err);

            res.json(user);
        });

    })

    // get all the users (accessed at GET http://localhost:8080/api/users)
    .get(function(req, res) {
        User.find(function(err, users) {
            if (err)
                res.send(err);

            res.json(users);
        });
    });

// on routes that end in /users/:user_id
// ----------------------------------------------------
users_router.route('/:user_id')

// get the user with that id (accessed at GET http://localhost:8080/api/users/:user_id)
    .get(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err)
                res.send(err);
            res.json(user);
        })
            .populate("training");
    })

    // update the user with this id (accessed at PUT http://localhost:8080/api/users/:user_id)
    .put(function(req, res) {

        // use our user model to find the user we want
        User.findById(req.params.user_id, function(err, user) {

            if (err)
                res.send(err);

            user.password = req.body.password;
            user.email = req.body.email;
            user.username = req.body.username;
            user.role = req.body.role;
            user.created = req.body.created;
            user.gravatar = "http://www.gravatar.com/avatar/" + md5(user.email) + "?d=identicon&s=48";
            user.training = req.body.training._id;
            user.favorites = req.body.favorites;

            // save the user
            user.save(function(err) {
                if (err)
                    res.send(err);

                res.json(user);
            });

        });
    })

    // update the user with this id (accessed at PATCH http://localhost:8080/api/users/:user_id)
    .patch(function(req, res) {

        // use our user model to find the user we want
        User.findById(req.params.user_id, function(err, user) {

            for(var key in req.body) {
                if(req.body.hasOwnProperty(key)){
                    user[key] = req.body[key];
                }
            }

            if (err)
                res.send(err);

            // save the user
            user.save(function(err) {
                if (err)
                    res.send(err);

                res.json(user);
            });

        });
    })

    // delete the user with this id (accessed at DELETE http://localhost:8080/api/users/:user_id)
    .delete(function(req, res) {
        User.remove({
            _id: req.params.user_id
        }, function(err) {
            if (err)
                res.send(err);

            res.json(req.params.user_id);
        });
    });

// on routes that end in /users/:user_id
// ----------------------------------------------------
users_router.route('/favorites/:user_id')

// get the user with that id (accessed at GET http://localhost:8080/api/users/:user_id)
    .get(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err)
                res.send(err);
            res.json(user);
        })
            .populate("favorites");
    });

users_router.route('/login')
    .post(function(req, res) {
        console.log("try login")
        User.find(function(err, users) {
            if (err)
                res.send(err);
            var user = users.find(user => user.email === req.body.email);
            if (user != undefined){
                if (user.password === req.body.password){
                    console.log("OK !");
                    res.json(user);
                }
                else
                    res.status(500).send('Bad credentials');
            }
            else
                res.status(500).send('User not found');
        })
           /* .populate("training")*/;
    })

module.exports = users_router;