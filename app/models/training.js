// app/models/training.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var TrainingSchema   = new Schema({
    title: String,
    details: String,
    courses: [{type: Schema.Types.ObjectId, ref: 'Course'}]
});

module.exports = mongoose.model('Training', TrainingSchema);