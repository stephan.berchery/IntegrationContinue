// app/models/video.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var VideoSchema   = new Schema({
    title: String,
    created: String,
    owner: {type: Schema.Types.ObjectId, ref: 'User'},
    comment: String,
    url: String,
    tags: String,
    nbLikes: [String],
    thumbnail: String
});

module.exports = mongoose.model('Video', VideoSchema);