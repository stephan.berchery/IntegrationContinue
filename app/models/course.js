// app/models/course.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CourseSchema   = new Schema({
    title: String,
    details: String,
    videos: [{type: Schema.Types.ObjectId, ref: 'Video'}]

});

module.exports = mongoose.model('Course', CourseSchema);