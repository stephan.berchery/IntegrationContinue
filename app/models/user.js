// app/models/user.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
    email: String,
    password: String,
    username: String,
    role: String,
    created: String,
    gravatar: String,
    training: {type: Schema.Types.ObjectId, ref: 'Training'},
    favorites: [{type: Schema.Types.ObjectId, ref: 'Video'}]
});

module.exports = mongoose.model('User', UserSchema);